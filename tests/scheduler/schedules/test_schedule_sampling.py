# Repository: https://gitlab.com/quantify-os/quantify-scheduler
# Licensed according to the LICENCE file on the master branch
# pylint: disable=missing-function-docstring

import numpy as np
import pytest

from quantify_scheduler.compilation import determine_absolute_timing
from quantify_scheduler.pulse_library import SquarePulse
from quantify_scheduler.types import Schedule
from quantify_scheduler.visualization.pulse_diagram import sample_schedule


def test_sample_schedule() -> None:
    schedule = Schedule("test")
    r = SquarePulse(amp=0.2, duration=4e-9, port="SDP")
    schedule.add(r)
    rm = SquarePulse(amp=-0.2, duration=6e-9, port="T")
    schedule.add(rm, ref_pt="start")
    r = SquarePulse(amp=0.3, duration=6e-9, port="SDP")
    schedule.add(r)
    schedule.add(r)
    determine_absolute_timing(schedule=schedule)

    timestamps, waveforms = sample_schedule(schedule, sampling_rate=0.5e9)

    np.testing.assert_array_almost_equal(
        timestamps,
        np.array(
            [
                0.0e00,
                2.0e-09,
                4.0e-09,
                6.0e-09,
                8.0e-09,
                1.0e-08,
                1.2e-08,
                1.4e-08,
                1.6e-08,
            ]
        ),
    )

    np.testing.assert_array_almost_equal(
        waveforms["SDP"], np.array([0.2, 0.2, 0.2, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3])
    )
    np.testing.assert_array_almost_equal(
        waveforms["T"], np.array([-0.2, -0.2, -0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    )


def test_sample_custom_port_list() -> None:
    schedule = Schedule("test")
    r = SquarePulse(amp=0.2, duration=4e-9, port="SDP")
    schedule.add(r)
    determine_absolute_timing(schedule=schedule)

    timestamps, waveforms = sample_schedule(
        schedule, sampling_rate=0.5e9, port_list=["SDP"]
    )
    assert list(waveforms.keys()) == ["SDP"]


def test_sample_empty_schedule() -> None:
    schedule = Schedule("test")

    with pytest.raises(TypeError):
        timestamps, waveforms = sample_schedule(schedule, sampling_rate=1e9)
